/* eslint-env mocha */

import fs from 'fs';
import path from 'path';
for (const mod of fs.readdirSync('src')) {
  if (mod.substr(mod.length - 3) === '.js') {
    require(path.join('../../src', mod));
  }
}
