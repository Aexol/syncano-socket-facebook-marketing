/* eslint-env mocha */
import { assert } from 'chai';
import { FacebookAdsApi } from 'facebook-nodejs-ads-sdk';
import * as campaigns from '../../src/campaigns';
import { getTestingEndpoint, mockEndpointContext } from '../syncano-helper';
import { MongoConnection } from '../../src/mongo';

const ENDPOINT = 'campaigns';
describe('integration campaigns', function () {
  this.timeout(30000);
  const accessToken = process.env.FACEBOOK_API_TOKEN;
  const accountId = process.env.FACEBOOK_ACCOUNT_ID;
  const mongoUrl = process.env.MONGO_URL;
  let conn;
  let campaign;
  FacebookAdsApi.init(accessToken);
  before(async () => {
    campaign = await campaigns.createFacebookCampaign(
      { name: 'prod' },
      'LINK_CLICKS',
      accountId
    );
    conn = await new MongoConnection({ MONGO_URL: mongoUrl }).open();
  });
  after(async () => {
    await conn.close();
  });
  it('create facebook campaign', async () => {
    assert.containsAllKeys(
      campaign,
      ['fb_campaign', 'name', 'product'],
      'new campaign created'
    );
  });
  it('create facebook adset', async () => {
    const adsets = await campaigns.createFacebookAds(
      campaign,
      {
        args: {
          billingEvent: 'IMPRESSIONS',
          bidAmount: 2,
          dailyBudget: 2000,
          targets: [
            {
              geo_locations: { countries: ['US'] },
              publisher_platforms: ['facebook']
            },
            {
              geo_locations: { countries: ['NZ'] },
              publisher_platforms: ['facebook']
            },
            {
              geo_locations: { countries: ['JP'] },
              publisher_platforms: ['facebook']
            },
            {
              geo_locations: { countries: ['SV'] },
              publisher_platforms: ['facebook']
            },
            {
              geo_locations: { countries: ['NO'] },
              publisher_platforms: ['facebook']
            },
            {
              geo_locations: { countries: ['PL'] },
              publisher_platforms: ['facebook']
            }
          ],
          creative: '120330000391286912'
        }
      },
      accountId
    );
    adsets.forEach(a =>
      assert.containsAllKeys(a, ['fb_ad_set', 'name', 'target'])
    );
    assert.deepEqual(adsets[0].target, {
      geo_locations: { countries: ['US'] },
      publisher_platforms: ['facebook']
    });
    assert.deepEqual(adsets[1].target, {
      geo_locations: { countries: ['NZ'] },
      publisher_platforms: ['facebook']
    });
    assert.deepEqual(adsets[2].target, {
      geo_locations: { countries: ['JP'] },
      publisher_platforms: ['facebook']
    });
    assert.deepEqual(adsets[3].target, {
      geo_locations: { countries: ['SV'] },
      publisher_platforms: ['facebook']
    });
    assert.deepEqual(adsets[4].target, {
      geo_locations: { countries: ['NO'] },
      publisher_platforms: ['facebook']
    });
    assert.deepEqual(adsets[5].target, {
      geo_locations: { countries: ['PL'] },
      publisher_platforms: ['facebook']
    });
  });
  it('list facebook campaigns all fields', async () => {
    const endpoint = getTestingEndpoint(ENDPOINT, {
      classes: ['campaigns']
    });
    const { middlewareStub } = endpoint;
    const { list, fields } = middlewareStub.syncano.data.campaigns;
    list.resolves([campaign]);
    fields.returnsThis();
    const res = await endpoint(
      mockEndpointContext(ENDPOINT, { args: { accessToken } })
    );
    assert.equal(res[0].id, campaign.fb_campaign);
  });
  it('create facebook campaign', async () => {
    const endpoint = getTestingEndpoint(ENDPOINT, {
      classes: ['ad_sets', 'campaigns', 'products']
    });
    const { middlewareStub } = endpoint;
    const {
      create: campaignsCreate,
      fields: campaignsFields
    } = middlewareStub.syncano.data.campaigns;
    const {
      create: adsetsCreate,
      fields: adsetsFields
    } = middlewareStub.syncano.data.ad_sets;
    const {
      findOrFail: productsFindOrFail,
      fields: productsFields
    } = middlewareStub.syncano.data.products;
    campaignsCreate.resolvesArg(0);
    campaignsFields.returnsThis();
    adsetsCreate.resolvesArg(0);
    adsetsFields.returnsThis();
    productsFindOrFail.resolves({ name: 'prod', id: 1 });
    productsFields.returnsThis();
    const targets = [
      {
        geo_locations: { countries: ['US'] },
        publisher_platforms: ['facebook']
      },
      {
        geo_locations: { countries: ['NZ'] },
        publisher_platforms: ['facebook']
      },
      {
        geo_locations: { countries: ['JP'] },
        publisher_platforms: ['facebook']
      },
      {
        geo_locations: { countries: ['PL'] },
        publisher_platforms: ['facebook']
      }
    ];
    const campaign = await endpoint(
      mockEndpointContext(ENDPOINT, {
        method: 'POST',
        args: {
          accessToken,
          accountId,
          product: 1,
          targets,
          objective: 'LINK_CLICKS',
          billingEvent: 'IMPRESSIONS',
          bidAmount: 2,
          dailyBudget: 2000,
          creative: '120330000391286912'
        },
        config: {
          MONGO_URL: mongoUrl
        }
      })
    );
    assert.containsAllKeys(campaign, ['id', 'name', 'adSets']);
    campaign.adSets.forEach(a =>
      assert.containsAllKeys(a, ['id', 'name', 'target'])
    );
    const filter = {
      product: 'prod',
      campaign: campaign.id
    };
    const mtargets = conn.db.collection('targets');
    const res = await mtargets.find(filter).toArray();
    targets
      .map((target, idx) => ({
        product: 'prod',
        campaign: campaign.id,
        target: target
      }))
      .forEach((v, i) => {
        assert.deepNestedInclude(res[i], v);
      });
    await mtargets.deleteMany(filter);
    assert.equal(res.length, targets.length);
  });
  it('mongo integration', async () => {
    const campaign = {
      fb_campaign: 2
    };
    const adSets = [
      {
        geo_locations: { countries: ['US'] },
        publisher_platforms: ['facebook']
      },
      {
        geo_locations: { countries: ['NZ'] },
        publisher_platforms: ['facebook']
      },
      {
        geo_locations: { countries: ['JP'] },
        publisher_platforms: ['facebook']
      },
      {
        geo_locations: { countries: ['PL'] },
        publisher_platforms: ['facebook']
      }
    ].map((target, idx) => ({
      fb_ad_set: idx + 3,
      target
    }));
    const config = {
      MONGO_URL: mongoUrl
    };
    await campaigns.makeMongoFilterObjects(
      adSets,
      campaign,
      { name: 'prod' },
      {
        config
      }
    );
    const filter = {
      product: 'prod',
      campaign: 2
    };
    const targets = conn.db.collection('targets');
    const res = await targets.find(filter).toArray();
    adSets
      .map((target, idx) => ({
        product: 'prod',
        campaign: 2,
        adset: idx + 3,
        target: target.target
      }))
      .forEach((v, i) => {
        assert.deepNestedInclude(res[i], v);
      });
    await targets.deleteMany(filter);
    assert.equal(res.length, adSets.length);
  });
});
