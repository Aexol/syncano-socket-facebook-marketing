/* eslint-env mocha */
import { assert } from 'chai';
import { FacebookAdsApi } from 'facebook-nodejs-ads-sdk';
import * as campaigns from '../../src/campaigns';
import { getTestingEndpoint, mockEndpointContext } from '../syncano-helper';

const ENDPOINT = 'campaign';
describe('campaign integration facebook', function () {
  this.timeout(30000);
  const accessToken = process.env.FACEBOOK_API_TOKEN;
  const accountId = process.env.FACEBOOK_ACCOUNT_ID;
  FacebookAdsApi.init(accessToken);
  let campaign;
  let adsets;
  before(async () => {
    campaign = await campaigns.createFacebookCampaign(
      { name: 'prod' },
      'LINK_CLICKS',
      accountId
    );
    adsets = await campaigns.createFacebookAds(
      campaign,
      {
        args: {
          billingEvent: 'IMPRESSIONS',
          bidAmount: 2,
          dailyBudget: 2000,
          targets: [
            {
              geo_locations: { countries: ['US'] },
              publisher_platforms: ['facebook']
            },
            {
              geo_locations: { countries: ['NZ'] },
              publisher_platforms: ['facebook']
            }
          ],
          creative: '120330000391286912'
        }
      },
      accountId
    );
  });
  it('get campaign', async () => {
    let id = 2;
    const endpoint = getTestingEndpoint(ENDPOINT, {
      classes: ['ad_sets', 'campaigns']
    });
    const { middlewareStub } = endpoint;
    const {
      findOrFail: campaignsFindOrFail,
      where: campaignsWhere,
      fields: campaignsFields
    } = middlewareStub.syncano.data.campaigns;
    const {
      list: adsetsList,
      where: adsetsWhere,
      fields: adsetsFields
    } = middlewareStub.syncano.data.ad_sets;
    campaignsFindOrFail.resolves(Object.assign({}, campaign, { id: 1 }));
    campaignsWhere.returnsThis();
    campaignsFields.returnsThis();
    adsetsList.resolves(adsets.map(a => Object.assign({}, a, { id: id++ })));
    adsetsWhere.returnsThis();
    adsetsFields.returnsThis();
    const actual = await endpoint(
      mockEndpointContext(ENDPOINT, {
        method: 'GET',
        args: {
          accessToken,
          id: campaign.fb_campaign
        }
      })
    );
    assert.containsAllKeys(actual, ['id', 'name', 'adSets']);
    assert.equal(actual.id, campaign.fb_campaign);
    actual.adSets.forEach(e =>
      assert.containsAllKeys(e, ['id', 'name', 'target'])
    );
  });
});
