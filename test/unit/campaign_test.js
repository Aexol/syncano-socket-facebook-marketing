/* eslint-env mocha */
import { assert } from 'chai';
import { mockEndpointContext, getTestingEndpoint } from '../syncano-helper';

const ENDPOINT = 'campaign';
describe(ENDPOINT, () => {
  const endpoint = getTestingEndpoint(ENDPOINT, {
    classes: ['campaigns', 'creatives']
  });
  const { middlewareStub } = endpoint;
  const { findOrFail, update } = middlewareStub.syncano.data.campaigns;
  const creative = { id: 10, file: 10 };
  const creatives = middlewareStub.syncano.data.creatives;
  creatives.findOrFail.resolves(creative);
  const { success, notFound } = middlewareStub.response;
  const reset = () => {
    findOrFail.reset();
    update.reset();
    success.resetHistory();
    notFound.resetHistory();
    creatives.findOrFail.resetHistory();
  };
  const notFoundError = { response: { status: 404 } };

  beforeEach(() => {
    reset();
  });
  it('get', async () => {
    const ctx = mockEndpointContext(ENDPOINT, {
      args: {
        id: 5
      },
      method: 'GET'
    });
    findOrFail.resolves({
      id: ctx.args.id,
      creative: 10,
      target: { some: { filter: {} } }
    });
    let r = await endpoint(ctx);
    assert.deepEqual(
      r,
      {
        id: ctx.args.id,
        creative,
        target: { some: { filter: {} } }
      },
      'equals findOrFail result'
    );
    assert.isOk(findOrFail.calledOnce, 'findOrFail called');
    assert.isOk(findOrFail.calledWithExactly(5), 'findOrFail called with 5');
    assert.isOk(success.calledOnce, 'success called');
    assert.isOk(update.notCalled, 'update not called');
    assert.isOk(notFound.notCalled, 'notFound not called');

    reset();

    findOrFail.rejects(notFoundError);
    r = await endpoint(ctx);
    assert.deepEqual(r, notFoundError.response, 'equals not found response');
    assert.isOk(findOrFail.calledOnce, 'findOrFail called');
    assert.isOk(findOrFail.calledWithExactly(5), 'findOrFail called with 5');
    assert.isOk(success.notCalled, 'success called');
    assert.isOk(update.notCalled, 'update not called');
    assert.isOk(notFound.calledOnce, 'notFound not called');
  });
  it('post', async () => {
    const ctx = mockEndpointContext(ENDPOINT, {
      args: {
        id: 5,
        targets: [],
        creative: 10,
        product: 10
      },
      method: 'POST'
    });
    update.resolves(ctx.args);
    let r = await endpoint(ctx);
    assert.deepEqual(r, ctx.args, 'equals update result');
    assert.isOk(findOrFail.notCalled, 'findOrFail not called');
    assert.isOk(success.calledOnce, 'success called');
    assert.isOk(update.calledOnce, 'update called');
    assert.isOk(
      update.calledWithExactly(5, { targets: [], creative: 10, product: 10 }),
      'update with'
    );
    assert(notFound.notCalled, 'notFound not called');

    reset();
    update.rejects(notFoundError);
    r = await endpoint(ctx);
    assert.deepEqual(r, notFoundError.response, 'equals update result');
    assert.isOk(findOrFail.notCalled, 'findOrFail not called');
    assert.isOk(success.notCalled, 'success not called');
    assert.isOk(update.calledOnce, 'update called');
    assert.isOk(
      update.calledWithExactly(5, { targets: [], creative: 10, product: 10 }),
      'update with'
    );
    assert(notFound.calledOnce, 'notFound called');
  });
});
