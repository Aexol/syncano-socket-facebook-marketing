/* eslint-env mocha */
import { assert } from 'chai';
import { spy } from 'sinon';
import { allowedMethods } from '../../src/utils';

describe('utils', () => {
  it('checks if method is allowed', () => {
    const endpoint = spy();
    const ctx = {
      meta: {
        request: {
          REQUEST_METHOD: 'GET'
        }
      }
    };
    const syncano = {};
    allowedMethods(endpoint, ['GET', 'POST'])(ctx, syncano);
    ctx.meta.request.REQUEST_METHOD = 'PUT';
    assert.isOk(endpoint.calledOnce);
    allowedMethods(endpoint, ['GET', 'POST'])(ctx, syncano);
    assert.isOk(endpoint.calledOnce);
    ctx.meta.request.REQUEST_METHOD = 'POST';
    allowedMethods(endpoint, ['GET', 'POST'])(ctx, syncano);
    assert.isOk(endpoint.calledTwice);
    assert.isOk(endpoint.calledWithExactly(ctx, syncano));
  });
});
