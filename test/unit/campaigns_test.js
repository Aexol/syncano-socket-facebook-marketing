/* eslint-env mocha */
import { assert } from 'chai';
import { mockEndpointContext, getTestingEndpoint } from '../syncano-helper';

const ENDPOINT = 'campaigns';
describe(ENDPOINT, () => {
  const endpoint = getTestingEndpoint(ENDPOINT, {
    classes: ['campaigns']
  });
  const { middlewareStub } = endpoint;
  const { list, create, where } = middlewareStub.syncano.data.campaigns;
  where.returnsThis();
  const { success } = middlewareStub.response;
  const reset = () => {
    list.reset();
    create.reset();
    success.resetHistory();
  };
  beforeEach(() => {
    reset();
  });
  it('get', async () => {
    const ctx = mockEndpointContext(ENDPOINT, {
      method: 'GET'
    });
    const ret = [{ id: 1 }, { id: 2 }];
    list.resolves(ret);
    let r = await endpoint(ctx);
    assert.deepEqual(r, ret, 'list returned');
    assert.isOk(list.calledOnce, 'list called');
    assert.isOk(list.calledWithExactly(), 'list called');
    assert.isOk(success.calledOnce, 'success called');
    assert.isOk(create.notCalled, 'create not called');
  });
  it('post', async () => {
    list.resolves([]);
    const ctx = mockEndpointContext(ENDPOINT, {
      args: {
        targets: [{ a: '' }, { b: '' }],
        creative: 5,
        product: 5
      },
      method: 'POST'
    });
    let i = 0;
    const res = ctx.args.targets.map(t =>
      Object.assign({
        id: ++i,
        product: ctx.args.product,
        creative: ctx.args.creative,
        target: t,
        facebookCampaign: 1
      })
    );
    create.callsFake(campaigns => {
      for (const i in campaigns) {
        campaigns[i].id = +i + 1;
      }
      return Promise.resolve(campaigns);
    });
    let r = await endpoint(ctx);
    assert.deepEqual(r, res, 'new campaign returned');
    assert.isOk(list.notCalled, 'list not called');
    assert.isOk(success.calledOnce, 'success called');
    assert.isOk(create.calledOnce, 'create called');
    assert.isOk(create.calledWithExactly(ctx.args), 'create with args');
  });
});
