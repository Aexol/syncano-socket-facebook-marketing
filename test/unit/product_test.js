/* eslint-env mocha */
import { assert } from 'chai';
import { mockEndpointContext, getTestingEndpoint } from '../syncano-helper';

const ENDPOINT = 'product';
const PRODUCT_ID = 10;
describe(ENDPOINT, () => {
  const endpoint = getTestingEndpoint(ENDPOINT, {
    classes: ['products', 'campaigns', 'creatives']
  });
  const { middlewareStub } = endpoint;
  const { products, campaigns, creatives } = middlewareStub.syncano.data;
  campaigns.list.resolves([
    {
      id: 10,
      product: PRODUCT_ID,
      creative: 10,
      target: {
        tt: 10
      }
    },
    {
      id: 20,
      product: PRODUCT_ID,
      creative: 20,
      target: {
        tt: 20
      }
    }
  ]);
  campaigns.where.returnsThis();
  creatives.findOrFail.resolves([
    {
      id: 10,
      file: 10
    },
    {
      id: 20,
      file: 10
    }
  ]);
  const { findOrFail, update } = products;
  const { success, notFound } = middlewareStub.response;
  const reset = () => {
    findOrFail.reset();
    update.reset();
    success.resetHistory();
    notFound.resetHistory();
    campaigns.list.resetHistory();
    creatives.findOrFail.resetHistory();
  };
  const notFoundError = { response: { status: 404 } };

  beforeEach(() => {
    reset();
  });

  it('get', async () => {
    const { where, firstOrFail } = middlewareStub.syncano.data.products;
    let ctx = mockEndpointContext(ENDPOINT, {
      args: {
        id: PRODUCT_ID
      },
      method: 'GET'
    });
    const ret = {
      id: ctx.args.id,
      name: 'name',
      campaigns: [
        {
          id: 10,
          product: PRODUCT_ID,
          creative: {
            id: 10,
            file: 10
          },
          target: {
            tt: 10
          }
        },
        {
          id: 20,
          product: PRODUCT_ID,
          creative: {
            id: 20,
            file: 10
          },
          target: {
            tt: 20
          }
        }
      ]
    };
    findOrFail.resolves({ id: PRODUCT_ID, name: 'name', campaigns: [10, 20] });
    let r = await endpoint(ctx);
    assert.deepEqual(r, ret);
    assert.isOk(findOrFail.calledWithExactly(PRODUCT_ID));
    assert.isOk(success.calledOnce);
    assert.isOk(findOrFail.calledOnce);
    assert.isOk(where.notCalled);
    assert.isOk(firstOrFail.notCalled);
    assert.isOk(notFound.notCalled);
    assert.isOk(update.notCalled);

    reset();

    findOrFail.rejects(notFoundError);
    r = await endpoint(ctx);
    assert.deepEqual(r, notFoundError.response);
    assert.isOk(findOrFail.calledWithExactly(PRODUCT_ID));
    assert.isOk(success.notCalled);
    assert.isOk(findOrFail.calledOnce);
    assert.isOk(notFound.calledOnce);
    assert.isOk(update.notCalled);

    reset();

    ctx = mockEndpointContext(ENDPOINT, {
      args: {
        name: 'name'
      },
      method: 'GET'
    });
    firstOrFail.resolves({ id: PRODUCT_ID, name: 'name', campaigns: [10, 20] });
    where.returnsThis();
    r = await endpoint(ctx);
    assert.deepEqual(r, ret);
    assert.isOk(where.calledWithExactly('name', ctx.args.name));
    assert.isOk(firstOrFail.calledWithExactly());
    assert.isOk(where.calledOnce);
    assert.isOk(success.calledOnce);
    assert.isOk(firstOrFail.calledOnce);
    assert.isOk(findOrFail.notCalled);
    assert.isOk(notFound.notCalled);
    assert.isOk(update.notCalled);
  });

  it('post', async () => {
    const ctx = mockEndpointContext(ENDPOINT, {
      args: {
        id: 5,
        name: 10
      },
      method: 'POST'
    });
    update.resolves(ctx.args);
    let r = await endpoint(ctx);
    assert.deepEqual(r, ctx.args);
    assert.isOk(update.calledWithExactly(5, { name: 10 }));
    assert.isOk(success.calledOnce);
    assert.isOk(update.calledOnce);
    assert.isOk(notFound.notCalled);
    assert.isOk(findOrFail.notCalled);

    reset();

    update.rejects(notFoundError);
    r = await endpoint(ctx);
    assert.deepEqual(r, notFoundError.response);
    assert.isOk(update.calledWithExactly(5, { name: 10 }));
    assert.isOk(notFound.calledOnce);
    assert.isOk(update.calledOnce);
    assert.isOk(success.notCalled);
    assert.isOk(findOrFail.notCalled);
  });
});
