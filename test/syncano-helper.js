import yaml from 'js-yaml';
import fs from 'fs';
import Data from '../node_modules/@syncano/core/lib/data';
import Endpoint from '../node_modules/@syncano/core/lib/endpoint';
import sinon, { stub } from 'sinon';
import proxyquire from 'proxyquire';
sinon.addBehavior('resolvesArg', (fake, n) =>
  fake.callsFake((...args) => {
    return Promise.resolve(args[n]);
  })
);

const socket = yaml.safeLoad(fs.readFileSync('socket.yml').toString());
export function mockEndpointContext (endpointName, opts = {}) {
  return {
    args: opts.args || {},
    config: opts.config || {},
    meta: {
      executor: 'mocha',
      name: endpointName,
      request: {
        REQUEST_METHOD: opts.method || 'GET'
      },
      metadata: socket.endpoints[endpointName]
    }
  };
}

export function getTestingEndpoint (endpointName, opts = {}) {
  const middlewareStub = stubMiddleware(opts);
  const endpoint = proxyquire(`../src/${endpointName}`, {
    '@aexol/syncano-middleware': middlewareStub,
    '@aexol/syncano-middleware-validate': {
      default: fn => (ctx, syncano) => fn(ctx, syncano)
    }
  }).default;
  endpoint.middlewareStub = middlewareStub;
  return endpoint;
}

export function mockSyncanoObj (opts = {}) {
  const syncano = {
    data: {},
    endpoint: stubEndpoint(),
    instance: {
      instance: {
        instanceName: 'test-run',
        spaceHost: 'localhost'
      }
    }
  };
  for (const clazz of opts.classes || []) {
    syncano.data[clazz] = stubData();
  }
  return syncano;
}

export function stubMiddleware (opts = {}) {
  const syncano = mockSyncanoObj(opts);
  const response = stub();
  response.success = stub();
  response.notFound = stub();
  response.json = stub();
  response.json.returnsArg(0);
  response.success.returnsArg(0);
  response.notFound.returnsArg(0);
  response.returnsArg(0);
  return {
    default: (() => {
      let serve = (ctx, fn) => {
        return fn(ctx, syncano);
      };
      return serve;
    })(),
    response,
    syncano
  };
}

export function stubProps (props) {
  return props.reduce((acc, val) => {
    acc[val] = stub();
    return acc;
  }, {});
}

export function filterBad (props, obj) {
  return props
    .filter(v => typeof obj[v] === 'function')
    .filter(v => v[0] !== '_')
    .filter(
      v =>
        ![
          'constructor',
          'toString',
          'toLocaleString',
          'valueOf',
          'isPrototypeOf',
          'hasOwnProperty',
          'propertyIsEnumerable'
        ].find(exclude => v === exclude)
    );
}

export function stubObject (obj) {
  let props = [];
  do {
    props = props.concat(filterBad(Object.getOwnPropertyNames(obj), obj));
    obj = Object.getPrototypeOf(obj);
  } while (obj);
  return stubProps(props);
}

export function stubData () {
  return stubObject(new Data({}));
}

export function stubEndpoint () {
  return stubObject(new Endpoint({}));
}
