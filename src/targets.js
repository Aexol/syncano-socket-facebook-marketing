import serve, { response, cleanExit } from '@aexol/syncano-middleware';
import validate from '@aexol/syncano-middleware-validate';
import { allowedMethods } from '@aexol/syncano-middleware-common';
import {
  randomValueHex,
  handleFacebookError,
  cleanUpError,
  getDB
} from './utils';
import { FacebookAdsApi, AdAccount, AdSet, Ad } from 'facebook-nodejs-ads-sdk';

var Promise = require('bluebird');

export async function createFacebookAd (campaign, ctx, accountId, logger) {
  const target = ctx.args.target;
  const name = campaign.name + '-adset-' + randomValueHex(6);
  const creative = target.creative || ctx.args.creative;
  delete target.creative;
  const accId = `act_${accountId}`;
  const adset = await new AdAccount(accId).createAdSet([], {
    [AdSet.Fields.name]: name,
    [AdSet.Fields.campaign_id]: campaign.fb_campaign,
    [AdSet.Fields.billing_event]: ctx.args.billingEvent,
    [AdSet.Fields.bid_amount]: ctx.args.bidAmount,
    [AdSet.Fields.daily_budget]: ctx.args.dailyBudget,
    [AdSet.Fields.targeting]: target,
    [AdSet.Fields.status]: AdSet.Status.paused
  });
  await new AdAccount(accId)
    .createAd([], {
      [Ad.Fields.name]: adset.name + '-ad',
      [Ad.Fields.adset_id]: adset.id,
      [Ad.Fields.creative]: {
        creative_id: creative
      },
      [AdSet.Fields.status]: Ad.Status.active
    })
    .catch(async e => {
      await new AdSet(adset.id).delete([], {});
      throw e;
    });
  return { fb_ad_set: adset.id, name, campaign: campaign.id, target };
}

export function makeMongoFilterObjects (adSets, campaign, product, ctx, logger) {
  return Promise.using(getDB(ctx.config), db =>
    db.collection('targets').insertMany(
      adSets.map(adSet => ({
        product: product.name,
        campaign: campaign.fb_campaign,
        adset: adSet.fb_ad_set,
        target: adSet.target
      }))
    )
  ).catch(e => {
    logger.error(e.message);
    throw e;
  });
}

export async function post (ctx, syncano) {
  const logger = syncano.logger(ctx.meta.executor);
  const { accessToken, accountId } = ctx.args;
  delete ctx.args.accessToken;
  delete ctx.args.accountId;
  FacebookAdsApi.init(accessToken);
  let campaign;
  try {
    campaign = await syncano.data.campaigns
      .fields('id', 'fb_campaign', 'product', 'name')
      .where('fb_campaign', 'eq', ctx.args.campaign)
      .firstOrFail();
  } catch (e) {
    return response.notFound({
      message: `campaign ${ctx.args.campaign} not found`
    });
  }
  let product;
  try {
    product = await syncano.data.products
      .fields('name')
      .findOrFail(campaign.product);
  } catch (e) {
    return response.notFound({
      message: `product ${campaign.product} not found`
    });
  }
  try {
    const adSet = await createFacebookAd(campaign, ctx, accountId, logger)
      .then(a =>
        Promise.all([
          syncano.data.ad_sets.fields('fb_ad_set').create(a),
          makeMongoFilterObjects([a], campaign, product, ctx, logger)
        ])
      )
      .then(v => ({ id: v[0].fb_ad_set }))
      .then(a => {
        return a;
      });
    syncano.event.emit('target_created', adSet);
    syncano.channel.publish(`create-trace.${product.name}`, {
      op: 'target-create',
      target: adSet,
      result: 'success'
    });
    return response.success(adSet);
  } catch (e) {
    const args = ctx.args;
    args.accessToken = 'hidden';
    syncano.channel.publish(`create-trace.${product.name}`, {
      op: 'target-create',
      args,
      result: 'failure',
      error: cleanUpError(e, accessToken)
    });
    throw e;
  }
}

export default ctx =>
  serve(
    ctx,
    cleanExit(
      handleFacebookError(
        allowedMethods({
          POST: validate(post)
        })
      )
    )
  );
