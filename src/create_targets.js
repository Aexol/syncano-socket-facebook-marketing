import serve, { cleanExit, response } from '@aexol/syncano-middleware';
import { post as createTarget } from './targets';
import { handleFacebookError } from './utils';

async function run (ctx, syncano) {
  const logger = syncano.logger(
    `${ctx.meta.socket}/create-targets-event-handler`
  );
  if (ctx.args.targets.length === 0) {
    return;
  }
  const [target] = ctx.args.targets.splice(0, 1);
  const creative = target.creative;
  const newCtx = Object.assign({}, ctx, {
    args: Object.assign({}, ctx.args)
  });
  newCtx.args.target = target;
  newCtx.args.creative = creative;
  newCtx.meta.executor = `${ctx.meta.socket}/targets`;
  delete target.creative;
  delete newCtx.args.targets;
  try {
    const r = await handleFacebookError(createTarget)(newCtx, syncano);
    if (r.responseName !== 'success' && r.status !== 200) {
      logger.error(r.payload ? JSON.stringify(r.payload) : r.message);
    }
  } catch (e) {
    logger.error(JSON.stringify(e));
  }
  if (ctx.args.targets.length > 0) {
    syncano.event.emit('create_targets', ctx.args);
  }
  return response.json({ message: 'success' });
}

export default ctx => serve(ctx, cleanExit(run));
