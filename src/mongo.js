import { MongoClient } from 'mongodb';

let clients = {};
let dbs = {};

export class MongoConnection {
  constructor (config = {}) {
    this.url = config.MONGO_URL;
  }

  get url () {
    return this.mongoUrl;
  }

  set url (val) {
    this.mongoUrl = val;
    if (val) {
      this.mongoDb = this.mongoUrl.split('/')[3];
    } else {
      this.mongoDb = undefined;
    }
  }

  async open () {
    if (!this.mongoDb || !this.mongoUrl) {
      return;
    }
    if (dbs[this.mongoDb] || clients[this.mongoUrl]) {
      return this;
    }
    const url = this.mongoUrl;
    const conn = this;
    return MongoClient.connect(url)
      .then(cli => {
        clients[url] = cli;
      })
      .then(() => conn);
  }

  close () {
    if (this.mongoUrl && clients[this.mongoUrl]) {
      return clients[this.mongoUrl].close();
    }
  }

  get db () {
    if (dbs[this.mongoDb]) {
      return dbs[this.mongoDb];
    }

    if (clients[this.mongoUrl]) {
      dbs[this.mongoDb] = clients[this.mongoUrl].db(this.mongoDb);
      return dbs[this.mongoDb];
    }
    return undefined;
  }

  get dbName () {
    return this.mongoDb;
  }

  set dbName (val) {
    this.mongoDb = val;
  }
}
