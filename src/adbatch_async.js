import { FacebookAdsApiBatch } from 'facebook-nodejs-ads-sdk';

export class AsyncFacebookAdsApiBatch extends FacebookAdsApiBatch {
  constructor (accountId, api) {
    super(api);
    if (accountId.substring(0, 4) !== 'act_') {
      accountId = 'act_' + accountId;
    }
    this._accountId = accountId;
  }

  add (relativePath, params, name, files, request) {
    const call = super.add(
      undefined,
      relativePath,
      params,
      files,
      undefined,
      undefined,
      request
    );
    if (name) {
      call.name = name;
    }
    delete call.method;
    return call;
  }

  execute (params = {}) {
    if (this._batch.length < 1) {
      return;
    }

    const method = 'POST';
    const path = [this._accountId, 'async_batch_requests']; // request to root domain for a batch request
    if (!params.adbatch) {
      params.adbatch = this._batch;
    }

    // Call to the batch endpoint (WIP)
    return this._api.call(method, path, params);
  }
}
