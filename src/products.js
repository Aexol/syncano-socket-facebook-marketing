import serve, { response } from '@aexol/syncano-middleware';
import validate from '@aexol/syncano-middleware-validate';
import { allowedMethods } from '@aexol/syncano-middleware-common';

function get (ctx, syncano) {
  return syncano.data.products
    .fields('id', 'name')
    .list()
    .then(l => response.success(l));
}

function post (ctx, syncano) {
  return syncano.data.products
    .fields('id', 'name')
    .create(ctx.args)
    .then(p => response.success(p));
}

const middlewareChain = fn => validate(fn);
export default ctx =>
  serve(
    ctx,
    allowedMethods({
      POST: middlewareChain(post),
      GET: middlewareChain(get)
    })
  );
