import serve, { response, cleanExit } from '@aexol/syncano-middleware';
import {
  parseGETFields,
  allowedMethods
} from '@aexol/syncano-middleware-common';
import validate from '@aexol/syncano-middleware-validate';
import {
  randomValueHex,
  handleFacebookError,
  cleanUpError,
  getDB
} from './utils';
import {
  FacebookAdsApiBatch,
  FacebookAdsApi,
  AdAccount,
  Campaign
} from 'facebook-nodejs-ads-sdk';

var Promise = require('bluebird');

function FbAdAccount (accountId) {
  return new AdAccount('act_' + accountId);
}

async function get (ctx, syncano) {
  const api = new FacebookAdsApiBatch(FacebookAdsApi.getDefaultApi());
  const { fields = ['id'], query } = ctx.args;
  const campaignsQuery = syncano.data.campaigns;
  if (query) {
    await Promise.using(getDB(ctx.config), async db => {
      const targets = db.collection('targets');
      const campaignIds = (await targets.find(query).toArray()).map(
        c => c.campaign
      );
      campaignsQuery.whereIn('fb_campaign', campaignIds);
    });
  }
  const campaigns = await campaignsQuery
    .fields('id', 'name', 'fb_campaign')
    .list();
  if (fields.length === 0 || (fields.length === 1 && fields[0] === 'id')) {
    return response.success(
      campaigns.map(c => ({
        id: c.fb_campaign,
        name: c.name
      }))
    );
  }
  const success = [];
  const failure = [];
  for (const c of campaigns) {
    api.add(
      'GET',
      c.fb_campaign,
      { fields: fields.join() },
      undefined,
      r => {
        success.push(r.body);
      },
      f => {
        failure.push(f.body);
      }
    );
  }
  await api.execute();
  if (failure.length !== 0) {
    throw failure;
  }
  return response.success(success);
}

export async function createFacebookAds (campaign, ctx, syncano, auth) {
  const newArgs = Object.assign({}, ctx.args, auth);
  delete newArgs.objective;
  newArgs.targets = newArgs.targets.map(t => {
    const creative = t.creative || newArgs.creative;
    t.creative = creative;
    return t;
  });
  newArgs.campaign = campaign.fb_campaign;
  syncano.event.emit('create_targets', newArgs);
}

export async function createFacebookCampaign (product, objective, accountId) {
  const name = product.name + '-campaign-' + randomValueHex(6);
  const campaign = await FbAdAccount(accountId).createCampaign([], {
    [Campaign.Fields.name]: name,
    [Campaign.Fields.status]: Campaign.Status.paused,
    [Campaign.Fields.objective]: objective
  });
  return {
    name,
    product: product.id,
    fb_campaign: campaign.id
  };
}

async function post (ctx, syncano) {
  const { accessToken, accountId } = ctx.args;
  delete ctx.args.accessToken;
  delete ctx.args.accountId;
  FacebookAdsApi.init(accessToken);
  const product = await syncano.data.products
    .fields('id', 'name')
    .findOrFail(ctx.args.product);
  try {
    const campaign = await createFacebookCampaign(
      product,
      ctx.args.objective,
      accountId
    ).then(campaign =>
      syncano.data.campaigns
        .fields('id', 'name', 'fb_campaign')
        .create(campaign)
    );
    syncano.event.emit('campaign_created', { id: campaign.fb_campaign });
    syncano.channel.publish(`create-trace.${product.name}`, {
      op: 'campaign-create',
      campaign,
      result: 'success'
    });
    createFacebookAds(campaign, ctx, syncano, { accountId, accessToken });
    return response.success({ id: campaign.fb_campaign, name: campaign.name });
  } catch (e) {
    const args = ctx.args;
    args.accessToken = 'hidden';
    syncano.channel.publish(`create-trace.${product.name}`, {
      op: 'campaign-create',
      args,
      result: 'failure',
      error: cleanUpError(e, accessToken)
    });
    throw e;
  }
}

const middlewareChain = fn => parseGETFields(validate(fn));
export default ctx =>
  serve(
    ctx,
    cleanExit(
      handleFacebookError(
        allowedMethods({
          GET: middlewareChain(get),
          POST: middlewareChain(post)
        })
      )
    )
  );
