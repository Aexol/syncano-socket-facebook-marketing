import { response } from '@aexol/syncano-middleware';
import crypto from 'crypto';
import { MongoClient } from 'mongodb';
var Promise = require('bluebird');

export function objectListToId (obj) {
  if (!Array.isArray(obj)) {
    obj = [obj];
  }
  return obj.reduce((acc, val) => {
    acc[val.id] = val;
    return acc;
  }, {});
}

export function randomValueHex (len) {
  return crypto
    .randomBytes(Math.ceil(len / 2))
    .toString('hex') // convert to hexadecimal format
    .slice(0, len)
    .toUpperCase(); // return required number of characters
}

export function checkFacebookRequestError (e) {
  return e.name === 'FacebookRequestError';
}

export function handleFacebookError (fn) {
  return async (ctx, syncano) => {
    return fn(ctx, syncano).catch(e => {
      if (checkFacebookRequestError(e)) {
        return response.json({ message: e.message }, e.status);
      }
      throw e;
    });
  };
}

export function cleanUpError (e, accessToken) {
  if (checkFacebookRequestError(e)) {
    if (e.url) {
      e.url = e.url.replace(accessToken, 'hidden');
    }
  }
  return e;
}

export function getDB (config) {
  let conn;
  const dbName = config.MONGO_URL.split('/')[3];
  return new Promise((resolve, reject) =>
    MongoClient.connect(config.MONGO_URL)
      .then(c => {
        conn = c;
        return resolve(c.db(dbName));
      })
      .catch(e => reject(e))
  ).disposer(() => {
    if (conn) conn.close();
  });
}
