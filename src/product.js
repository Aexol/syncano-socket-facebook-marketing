import serve, { response } from '@aexol/syncano-middleware';
import validate from '@aexol/syncano-middleware-validate';
import {
  allowedMethods,
  parseGETFields
} from '@aexol/syncano-middleware-common';

export async function join (product, syncano) {
  const campaigns = await syncano.data.campaigns
    .fields('id', 'name', 'fb_campaign')
    .where('product', product.id)
    .list()
    .then(campaigns =>
      campaigns.map(c => ({
        id: c.fb_campaign,
        name: c.name
      }))
    );
  return Object.assign({}, product, { campaigns });
}

function get (ctx, syncano) {
  const products = syncano.data.products.fields('id', 'name');
  const byId = ({ id }) => products.findOrFail(id);
  const byName = ({ name }) => products.where('name', name).firstOrFail();
  return (ctx.args.id ? byId : byName)(ctx.args)
    .then(p => join(p, syncano))
    .then(d => response.success(d))
    .catch(e => response.notFound(e.response));
}

function post (ctx, syncano) {
  const { name } = ctx.args;
  return syncano.data.products
    .fields('id', 'name')
    .update(ctx.args.id, { name })
    .then(d => response.success(d))
    .catch(e => response.notFound(e.response));
}

const middlewareChain = fn => parseGETFields(validate(fn));
export default ctx =>
  serve(
    ctx,
    allowedMethods({
      POST: middlewareChain(post),
      GET: middlewareChain(get)
    })
  );
