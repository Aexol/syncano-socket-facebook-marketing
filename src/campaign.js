import serve, { response } from '@aexol/syncano-middleware';
import validate from '@aexol/syncano-middleware-validate';
import {
  allowedMethods,
  parseGETFields
} from '@aexol/syncano-middleware-common';
import { FacebookAdsApiBatch, FacebookAdsApi } from 'facebook-nodejs-ads-sdk';

export async function cross (campaign, adSets) {
  return {
    name: campaign.name,
    id: campaign.fb_campaign,
    adSets: adSets.map(s => ({
      name: s.name,
      id: s.fb_ad_set,
      target: s.target
    }))
  };
}

export async function join (campaigns, syncano) {
  const isArray = Array.isArray(campaigns);
  if (!isArray) {
    campaigns = [campaigns];
  }
  const joinCampaigns = await Promise.all(
    campaigns.map(async campaign => {
      const adSets = await syncano.data.ad_sets
        .where('campaign', campaign.id)
        .fields('fb_ad_set', 'target', 'name')
        .list();
      return cross(campaign, adSets);
    })
  );
  return isArray ? joinCampaigns : joinCampaigns[0];
}

async function fillFacebookData (campaign, ctx) {
  const batch = new FacebookAdsApiBatch(FacebookAdsApi.getDefaultApi());
  const { campaignFields = ['id'], adsetFields = ['id'] } = ctx.args;
  const success = {};
  const onSuccess = s => {
    s = s.body;
    success[s.id] = s;
  };
  const failure = [];
  const onFailure = f => failure.push(f.body);
  const batchFetch = (o, fields) => {
    // Skip fetching for id.
    if (fields.length > 1 || fields[0] !== 'id') {
      batch.add('GET', o.id, { fields }, undefined, onSuccess, onFailure);
    } else {
      success[o.id] = o;
    }
  };
  batchFetch(campaign, campaignFields);
  campaign.adSets.forEach(adset => batchFetch(adset, adsetFields));
  await batch.execute();
  if (failure.length !== 0) {
    throw failure;
  }
  campaign = Object.assign(campaign, success[campaign.id]);
  campaign.adSets.forEach((adset, idx) => {
    campaign.adSets[idx] = Object.assign(adset, success[adset.id]);
  });
  return campaign;
}

function get (ctx, syncano) {
  return syncano.data.campaigns
    .where('fb_campaign', ctx.args.id)
    .fields('name', 'id', 'fb_campaign')
    .findOrFail()
    .then(r => join(r, syncano))
    .then(c => fillFacebookData(c, ctx))
    .then(r => response.success(r))
    .catch(e => response.notFound(e.response));
}

const middlewareChain = fn => parseGETFields(validate(fn));
export default ctx =>
  serve(
    ctx,
    allowedMethods({
      GET: middlewareChain(get)
    })
  );
