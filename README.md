# Socket facebook-marketing - ver 0.0.2

This socket is a simple socket that helps with creation and reviewing  facebook campaigns.


## Index

* [Config](#_config)
* [Classes](#_classes)
* [Endpoints](#_endpoints)
* [Events](#_events)



## Config

* [MONGO_URL](#_config-MONGO_URL)

|Options | Type | Required|
|--------|------|---------|
|MONGO_URL|    | Yes |

* MONGO_URL<a name="#_config-MONGO_URL"></a>

MongoDB helper for filtering. Url must contain all information needed to
connect to the database. That is instance url, db name and auth info.

Follows standard URI format: mongodb://[dbuser:dbpassword@]host:port/dbname




## Classes <a name='_classes'></a>

* [products](#_classes-products)
* [ad_sets](#_classes-ad_sets)
* [campaigns](#_classes-campaigns)

### products<a name='_classes-products'></a>

|Name | Type | filter_index | order_index | unique |
|-----|------|--------------|-------------|--------|
|name|string| Yes | No | Yes |
### ad_sets<a name='_classes-ad_sets'></a>

|Name | Type | filter_index | order_index | unique |
|-----|------|--------------|-------------|--------|
|name|string| No | No | No |
|campaign|[campaigns](#_classes-campaigns) reference| No | No | No |
|fb_ad_set|string| Yes | No | No |
|target|object| No | No | No |
### campaigns<a name='_classes-campaigns'></a>

|Name | Type | filter_index | order_index | unique |
|-----|------|--------------|-------------|--------|
|product|[products](#_classes-products) reference| No | No | No |
|name|string| No | No | No |
|fb_campaign|string| Yes | No | No |


## Endpoints <a name="_endpoints"></a>

* [facebook-marketing/campaign](#_endpoints-campaign)
* [facebook-marketing/campaigns](#_endpoints-campaigns)
* [facebook-marketing/create-trace](#_endpoints-create-trace)
* [facebook-marketing/product](#_endpoints-product)
* [facebook-marketing/products](#_endpoints-products)
* [facebook-marketing/targets](#_endpoints-targets)

#### campaign <span style="color:gray">*PRIVATE*</span><a name="_endpoints-campaign"></a>

Get or update campaign



###### GET

Get campaign by id

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Required</th>
  </tr>
  <tr>
    <td style="vertical-align:top">accessToken</td>
    <td style="vertical-align:top">string
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
  <tr>
    <td style="vertical-align:top">id</td>
    <td style="vertical-align:top">string
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
  <tr>
    <td style="vertical-align:top">campaignFields</td>
    <td style="vertical-align:top"><details><summary>array</summary>
    List of string

</details></td>
    <td style="vertical-align:top">No</td>
  </tr>
  <tr>
    <td style="vertical-align:top">adsetFields</td>
    <td style="vertical-align:top"><details><summary>array</summary>
    List of string

</details></td>
    <td style="vertical-align:top">No</td>
  </tr>
</table>


* accessToken

Valid facebook access token.

* id
* campaignFields

List of campaign fields to return.

* adsetFields

List of adset fields to return.



#### campaigns <span style="color:gray">*PRIVATE*</span><a name="_endpoints-campaigns"></a>

List all campaigns or create a new one.



###### GET

List all campaigns

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Required</th>
  </tr>
  <tr>
    <td style="vertical-align:top">accessToken</td>
    <td style="vertical-align:top">string
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
  <tr>
    <td style="vertical-align:top">fields</td>
    <td style="vertical-align:top"><details><summary>array</summary>
    List of string

</details></td>
    <td style="vertical-align:top">No</td>
  </tr>
  <tr>
    <td style="vertical-align:top">query</td>
    <td style="vertical-align:top">object
</td>
    <td style="vertical-align:top">No</td>
  </tr>
</table>


* accessToken

Valid Facebook Graph API Access token

* fields

List of campaign fields to return

* query

Mongo like object query. Root objects follow schema:
  properties:
    product:
      description: Product name
      type: string
    campaign:
      type: Numeric string with campaign id.
      type: string
    adset:
      type: Numeric string with adset id.
      type: string
    target:
      type: Targeting object.
      type: object



###### POST
<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Required</th>
  </tr>
  <tr>
    <td style="vertical-align:top">accessToken</td>
    <td style="vertical-align:top">string
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
  <tr>
    <td style="vertical-align:top">accountId</td>
    <td style="vertical-align:top">string
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
  <tr>
    <td style="vertical-align:top">billingEvent</td>
    <td style="vertical-align:top">

|Enum|
|----|
|APP_INSTALLS|
|CLICKS|
|IMPRESSIONS|
|LINK_CLICKS|
|NONE|
|OFFER_CLAIMS|
|PAGE_LIKES|
|POST_ENGAGEMENT|
|VIDEO_VIEWS|
</td>
    <td style="vertical-align:top">No</td>
  </tr>
  <tr>
    <td style="vertical-align:top">bidAmount</td>
    <td style="vertical-align:top">number
</td>
    <td style="vertical-align:top">No</td>
  </tr>
  <tr>
    <td style="vertical-align:top">dailyBudget</td>
    <td style="vertical-align:top">number
</td>
    <td style="vertical-align:top">No</td>
  </tr>
  <tr>
    <td style="vertical-align:top">objective</td>
    <td style="vertical-align:top">

|Enum|
|----|
|APP_INSTALLS|
|BRAND_AWARENESS|
|CONVERSIONS|
|EVENT_RESPONSES|
|LEAD_GENERATION|
|LINK_CLICKS|
|LOCAL_AWARENESS|
|MESSAGES|
|OFFER_CLAIMS|
|PAGE_LIKES|
|POST_ENGAGEMENT|
|PRODUCT_CATALOG_SALES|
|REACH|
|VIDEO_VIEWS|
</td>
    <td style="vertical-align:top">No</td>
  </tr>
  <tr>
    <td style="vertical-align:top">targets</td>
    <td style="vertical-align:top"><details><summary>array</summary>
    List of object

</details></td>
    <td style="vertical-align:top">Yes</td>
  </tr>
  <tr>
    <td style="vertical-align:top">creative</td>
    <td style="vertical-align:top">string
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
  <tr>
    <td style="vertical-align:top">product</td>
    <td style="vertical-align:top">number
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
</table>


* accessToken

Valid Facebook Graph API Access token

* accountId

Valid facebook account id

* billingEvent
* bidAmount
* dailyBudget
* objective
* targets

List of targets for which to create campaign

* creative

Creative for this campaign.

* product

Product id to which campaign belongs to.



#### create-trace <span style="color:gray">*PRIVATE*</span><a name="_endpoints-create-trace"></a>

Create campaign/target traces.



#### product <span style="color:gray">*PRIVATE*</span><a name="_endpoints-product"></a>

Get or update existing product



###### GET

Get product by id or name

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Required</th>
  </tr>
  <tr>
    <td style="vertical-align:top">oneOf</td>
    <td style="vertical-align:top">
</td>
    <td style="vertical-align:top">No</td>
  </tr>
</table>


* oneOf

###### POST
<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Required</th>
  </tr>
  <tr>
    <td style="vertical-align:top">id</td>
    <td style="vertical-align:top">number
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
  <tr>
    <td style="vertical-align:top">name</td>
    <td style="vertical-align:top">string
</td>
    <td style="vertical-align:top">No</td>
  </tr>
</table>


* id

Existing product id

* name

Product name



#### products <span style="color:gray">*PRIVATE*</span><a name="_endpoints-products"></a>

List all products or create new one.



###### GET

List all products


object



###### POST

Create new product


<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Required</th>
  </tr>
  <tr>
    <td style="vertical-align:top">name</td>
    <td style="vertical-align:top">string
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
</table>


* name

Product name



#### targets <span style="color:gray">*PRIVATE*</span><a name="_endpoints-targets"></a>

Create target for campaign


###### POST
<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Required</th>
  </tr>
  <tr>
    <td style="vertical-align:top">accessToken</td>
    <td style="vertical-align:top">string
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
  <tr>
    <td style="vertical-align:top">accountId</td>
    <td style="vertical-align:top">string
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
  <tr>
    <td style="vertical-align:top">campaign</td>
    <td style="vertical-align:top">string
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
  <tr>
    <td style="vertical-align:top">creative</td>
    <td style="vertical-align:top">string
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
  <tr>
    <td style="vertical-align:top">billingEvent</td>
    <td style="vertical-align:top">

|Enum|
|----|
|APP_INSTALLS|
|CLICKS|
|IMPRESSIONS|
|LINK_CLICKS|
|NONE|
|OFFER_CLAIMS|
|PAGE_LIKES|
|POST_ENGAGEMENT|
|VIDEO_VIEWS|
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
  <tr>
    <td style="vertical-align:top">bidAmount</td>
    <td style="vertical-align:top">number
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
  <tr>
    <td style="vertical-align:top">dailyBudget</td>
    <td style="vertical-align:top">number
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
  <tr>
    <td style="vertical-align:top">target</td>
    <td style="vertical-align:top">object
</td>
    <td style="vertical-align:top">Yes</td>
  </tr>
</table>


* accessToken

Valid Facebook Graph API Access token

* accountId

Valid facebook account id

* campaign

facebook campaign id

* creative
* billingEvent
* bidAmount
* dailyBudget
* target




## Events <a name="_events"></a>
